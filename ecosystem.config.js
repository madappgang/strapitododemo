module.exports = {
  apps : [{
    script: 'yarn start',
    env: {
      "DATABASE_FILENAME": "../data/data.db",
    }
  }],

  deploy : {
    production : {
      user : 'root',
      host : 'strapi.shouldworks.com',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:madappgang/strapitododemo.git',
      path : '/root/strapitododemo',
      'pre-deploy-local': '',
      'post-deploy' : 'yarn install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
