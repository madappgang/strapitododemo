'use strict';
const { sanitizeEntity } = require('strapi-utils');
const { TopFour } = require("../../../api_ts/build/topfour.js")

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {

    //three function returns top three todos for the user
    //sort by creation date desc
    //filter by user (requester)
    //filter by completion (not completed only)
    //limit to three
    async three(ctx) {
        //return 'I have not been implemented yet, help me!'
        const { id } = ctx.state.user;

        const todos =  await strapi.query('todo').find({ 
            _sort: 'created_at:asc',
            user: id,
            done: false,
            _limit: 3
        });

        return todos.map(entity => sanitizeEntity(entity, { model: strapi.models.todo }));
    },
    four: ctx => TopFour(ctx)

};
