'use strict';
const { sanitizeEntity } = require('strapi-utils');


/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {

    //overwrite default create function
    //to add requester user as an owner of todo beeing created
    async create(ctx) {
        let entity;
        const todo = ctx.request.body;
        todo.user = ctx.state.user.id;
        entity = await strapi.services.todo.create(todo);
        return sanitizeEntity(entity, { model: strapi.models.todo });
    },

    async find(ctx) {
        let entities;
        let query = {... ctx.query}
        query.user = ctx.state.user.id
        entities = await strapi.services.todo.find(query);
        return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.todo }));
    },
};
