'user strict';

const { sanitizeEntity } = require('strapi-utils');

export const TopFour = async (ctx: any) => {
    const { id } = ctx.state.user;

    const todos =  await strapi.query('todo').find({ 
        _sort: 'created_at:asc',
        user: id,
        done: false,
        _limit: 4
    });

    return todos.map( (entity:any) => sanitizeEntity(entity, { model: strapi.models.todo }));
}